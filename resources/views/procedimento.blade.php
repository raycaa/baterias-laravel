@extends('templates.base')

@section('conteudo')
    <main>
        <h1>Procedimentos</h1>
        <hr>
        <h2>Primeira etapa: </h2>
        <p>
            ▪️ Com a ajuda de um multimetro medimos a resistêcia do resistor de 22 ohms.
        </p>
        <p>
            ▪️ Depois medimos a tensão de cada pilha e bateria. 
        </p>
        <p>
            ▪️ Em seguida medimos a tensão junto com a corrente do resistor.  
        </p>
        <h3>Segunda etapa: </h3>
        <p>
            ▪️ Escrevemos o código HTML para calcular a resistencia interna de acordo com os valores obtidos.
        </p>
        <p>
            ▪️ Dividimos o codigo em pastas e adicionamos imagens.
        </p>
        <h4>Conclusão: </h4>
        <p>
            ▪️ Concluimos que quando a resistência interna é muito alta, a saúde da pilha não está boa e ela não funciona.
        </p>
        <img src="../imgs/Multímetro.jpg" width="200px">
    </main>
@endsection

@section('rodape')
    <h4>Rodapé do página procedimentos</h4>
@endsection